package nl.wimmelsoft.flightsim.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AirplaneTest {

    Airplane airplane;
    @Before
    public void Setup() {
        airplane = new Airplane();
    }

    @Test
    public void testChangeAltitudeWithPitchGreaterThanOne() {
        airplane.setForwardSpeed(10);
        airplane.changePitch(1);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertTrue(airplane.getVerticalSpeed() > 0);
    }
}