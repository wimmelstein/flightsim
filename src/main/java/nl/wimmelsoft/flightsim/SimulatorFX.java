package nl.wimmelsoft.flightsim;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import nl.wimmelsoft.flightsim.model.Airplane;
import nl.wimmelsoft.flightsim.view.MainPane;

import javax.swing.*;
import java.awt.event.ActionListener;

@Log
public class SimulatorFX extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        Airplane airplane = new Airplane();

        BorderPane root = new MainPane(airplane);

        root.setPrefSize(800, 400);
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("Flight Simulator");

        stage.show();
        scene.setOnKeyPressed(
                keyEvent -> {
                    switch (keyEvent.getCode()) {
                        case UP:
                            airplane.changePitch(+1);
                            break;
                        case DOWN:
                            airplane.changePitch(-1);
                            break;
                        case LEFT:
                            airplane.changeForwardSpeed(-10);
                            break;
                        case RIGHT:
                            airplane.changeForwardSpeed(10);
                            break;
                        case T:
                            airplane.getEngine().increaseRpm(10);
                            break;
                        default:
                            break;

                    }
                });

        ActionListener director =
                e -> fly(airplane);

        new Timer(1000, director).start();
    }

    private void fly(Airplane airplane) {

        if (airplane.getForwardSpeed() >= 180) {
            airplane.changeAltitude(airplane.getVerticalSpeed() - 10);
        }
        log.info(airplane.toString());
    }
}
