package nl.wimmelsoft.flightsim.view;

import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.GaugeBuilder;
import eu.hansolo.medusa.LcdDesign;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import lombok.extern.java.Log;
import nl.wimmelsoft.flightsim.model.Airplane;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

@Log
public class MainPane extends BorderPane implements PropertyChangeListener {

  private Gauge pitch, altitude, speed, verticalSpeed;

  public MainPane(Airplane airplane) {

    airplane.addPropertyChangeListener(this);
    airplane.getEngine().addPropertyChangeListener(this);
    pitch = GaugeBuilder.create()
            .title("pitch")
            .unit("degrees")
            .minValue(-30)
            .maxValue(30)
            .lcdDesign(LcdDesign.BLUE_BLACK)
            .lcdVisible(true)
            .build();

    altitude = GaugeBuilder.create()
            .title("Altitude")
            .minValue(0)
            .maxValue(30000)
            .unit("ft")
            .lcdDesign(LcdDesign.BLUE_BLACK)
            .lcdVisible(true)
            .build();

    verticalSpeed = GaugeBuilder.create()
            .title("Vertical Speed")
            .unit("ft/min")
            .minValue(-500)
            .maxValue(500)
            .lcdDesign(LcdDesign.BLUE_BLACK)
            .lcdVisible(true)
            .build();

    speed = GaugeBuilder.create()
            .title("Speed")
            .maxValue(1000)
            .minValue(0)
            .unit("kts")
            .lcdDesign(LcdDesign.BLUE_BLACK)
            .lcdVisible(true)
            .build();

    HBox box = new HBox();
    box.getChildren().addAll(pitch, verticalSpeed, altitude, speed);
    Label label = new Label("Wimmelsoft Flight Simulator");

    setCenter(box);
    setBottom(label);
    BorderPane.setAlignment(label, Pos.CENTER);
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    switch (evt.getPropertyName()) {
      case "pitch":
        pitch.setValue((Integer)evt.getNewValue());
        break;
      case "speed":
        speed.setValue((Integer)evt.getNewValue());
        break;
      case "altitude":
        altitude.setValue((Integer)evt.getNewValue());
        break;
      case "verticalSpeed":
        verticalSpeed.setValue((Integer)evt.getNewValue());
        break;
      case "rpm":
        log.info("RPM changed");
        break;
      default:
        break;
    }

  }
}
