package nl.wimmelsoft.flightsim.model;

import lombok.Data;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

@Data
public class Engine {

    private int rpm;
    private PropertyChangeSupport changes = new PropertyChangeSupport(this);

    public void increaseRpm(int n) {
        changes.firePropertyChange("rpm", rpm, rpm + n);
        rpm += n;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        changes.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        changes.removePropertyChangeListener(l);
    }
}
