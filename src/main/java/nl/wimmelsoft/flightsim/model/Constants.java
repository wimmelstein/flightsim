package nl.wimmelsoft.flightsim.model;

public interface Constants {
    int MAX_FORWARD_SPEED = 1000;
    int MAX_VERTICAL_SPEED = 45;
    int TAKE_OFF_SPEED = 180;
    int MAX_PITCH = 30;
    int MIN_PITCH = -30;
}
