package nl.wimmelsoft.flightsim.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.java.Log;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;

@Log
@Data
@NoArgsConstructor
public class Airplane implements PropertyChangeListener {

    private PropertyChangeSupport changes = new PropertyChangeSupport(this);



    private Engine engine = new Engine();

    private List<Wing> wings;

    private Tail tail;

    private int pitch, altitude, horizontalPosition = 0;
    private int forwardSpeed, verticalSpeed, crossSpeed = 0;

    public void changeVerticalSpeed(int n) {
        if (this.getAltitude() >= 0 && verticalSpeed < Constants.MAX_VERTICAL_SPEED) {
            changes.firePropertyChange("verticalSpeed", verticalSpeed, verticalSpeed + n);
            setVerticalSpeed(verticalSpeed + n);
        }
    }

    public void changeForwardSpeed(int n) {
        if ((n > 0 && forwardSpeed < Constants.MAX_FORWARD_SPEED) || (n < 0 && forwardSpeed > 0)) {
            forwardSpeed += n;
            changes.firePropertyChange("speed", forwardSpeed, forwardSpeed + n);
            setForwardSpeed(forwardSpeed + n);

        }
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        changes.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        changes.removePropertyChangeListener(l);
    }

    public void changeAltitude(int n) {
        if ((n < 0 && altitude > 0) || n > 0 && altitude >= 0) {
            changes.firePropertyChange("altitude", altitude, altitude + n);
            setAltitude(altitude + n);
        }

    }

    public void changePitch(int n) {
        if (isPitchWithinLimits(n)) {
            changes.firePropertyChange("pitch", pitch, pitch +n);
            changeVerticalSpeed(n * 10);
            setPitch(pitch + n);
        }
    }

    private boolean isPitchWithinLimits(int n) {
        return (n > 0 && pitch < Constants.MAX_PITCH) || (n < 0 && pitch > Constants.MIN_PITCH);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        // Needed when child objects change
    }
}
